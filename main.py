from bs4 import BeautifulSoup
import requests
import datetime

ct = datetime.datetime.now()
ts = ct.timestamp()

f_name=str(ts)+'  1730935.txt'
request = requests.get('https://stackoverflow.com/jobs')
soup =BeautifulSoup(request.text, 'lxml')
soup.prettify()

jobs=soup.find_all('h2', class_='mb4 fc-black-800 fs-body3')
for index,job in enumerate(jobs):
    job_title=job.find('a', class_='s-link stretched-link').text
    #print(job_title)

    with open(f'posts/{index}.' + f_name, 'w') as f:
        f.write(job_title+'\n')
        #print(job_title)


companys = soup.find_all('h3', class_='fc-black-700 fs-body1 mb4')

for index,company in enumerate(companys):
    com_name=company.find('span').text.replace(' ','')
    #print(com_name)
    com_country = company.find('span', class_="fc-black-500").text.replace(' ','')
    #print(com_country)

    with open(f'posts/{index}.'+f_name,'a')as f:
        f.write(com_name+'\n')
        f.write(com_country)

            #print(com_country)
            #print(com_name)